
extends Area2D

# member variables here, example:
# var a=2
# var b="textvar"
var leftNumb = 0
var rightNumb = 0
var answerEnemy = -1
var operator = -1
var pos = Vector2()
var maxNumb = 10
var name
var level
var middleNumb = 0
var answerEnemy2 = -1
var speed = 0
var loop = 0
var mode
var random

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_tree().set_pause(false)
	level = get_node("/root/var").lv
	get_node("../player").enemyCount += 1
	speed = get_node("/root/var").speed
	loop = get_node("/root/var").loop
	maxNumb = get_node("/root/var").maxNumb
	mode = get_node("/root/var").mode
	
##################################################
	#max number allowed each level
###################################################
	
	if (level >7) :
		maxNumb = 20
	name = int(get_name())
	randomize()
	
########################################################
	#level setting
#########################################################
	
	if (mode == 0):
		if (level <=4 ):
			while (1 > answerEnemy or answerEnemy > maxNumb or not operator == 0 ):
				random()
		if (level > 5 and level <= 10 ):
			while ( 1 > answerEnemy or answerEnemy > maxNumb or not operator == 0):
				random2()
		else :
			random = round(rand_range(1,2))
			if (random == 1 ):
				while (1 > answerEnemy or answerEnemy > maxNumb or not operator == 0 ):
					random()
			else :
				while (1 > answerEnemy or answerEnemy > maxNumb or not operator == 0 ):
					random2()
	if (mode == 1):
		if (level <=4 ):
			while (1 > answerEnemy or answerEnemy > maxNumb or not operator == 1 ):
				random()
		if (level > 5 and level <= 10 ):
			while (1 > answerEnemy2 or 1 > answerEnemy or answerEnemy > maxNumb or not operator == 1):
				random2()
		else :
			random = round(rand_range(1,2))
			if (random == 1 ):
				while (1 > answerEnemy or answerEnemy > maxNumb or not operator == 1 ):
					random()
			else :
				while (1 > answerEnemy or answerEnemy > maxNumb or not operator == 1 ):
					random2()

	
	set_process(true)
	
####################################################
	#answer to global array
#####################################################

	get_node("../player").enemyList.insert(get_node("../player").enemyList.size(),int(answerEnemy))
	pass

func _process(delta):
	pos = get_pos()
	pos += Vector2(0, speed)
	set_pos(pos)


func _on_enemy_area_enter( area ):
	
	
###########################################################
	#remove enemy on hit
##########################################################

	if (area.has_method("_on_Bullet_area_enter")):
		get_node("../player").enemyList.remove(name - 1)
		get_node("../player").enemyList.insert(name - 1,0)
		get_node("anim").play("die")
		get_node("../player").enemyDestroy += 1

##############################################
	#remove enemy on floor
###############################################	
	
	else :
		get_node("../player").enemyList.remove(name - 1)
		get_node("../player").enemyList.insert(name - 1,0)
		if ( get_node("/root/var").life != 0 ):
			get_node("../heart" + str(get_node("/root/var").life)).queue_free()
			get_node("/root/var").life -= 1
			get_node("anim").play("die")
		else :
			get_node("/root/var").last -= 1
			get_node("/root/var").level()
			get_node("../anim").play("gameOver")
			get_tree().set_pause(true)

	pass # replace with function body

func random():
	
######################################################
	#randomize number, 1 operator
######################################################
	
	leftNumb = round(rand_range(0,maxNumb))
	get_node("leftNumb").set_text(str(leftNumb))
	rightNumb = round(rand_range(0,maxNumb))
	get_node("rightNumb").set_text(str(rightNumb))
	operator = round(rand_range(0,1))
	if operator == 0 :
		get_node("operate").set_text("+")
		answerEnemy = leftNumb + rightNumb
	if operator == 1 :
		get_node("operate").set_text("-")
		answerEnemy = leftNumb - rightNumb
	

func random2():
	
########################################################
	#randomzie number, 2 operator
########################################################
	
	get_node("operate3").show()
	get_node("operate2").show()
	leftNumb = round(rand_range(0,maxNumb))
	get_node("operate2").set_text(str(leftNumb))
	rightNumb = round(rand_range(0,maxNumb))
	get_node("operate3").set_text(str(rightNumb))
	middleNumb = round(rand_range(0,maxNumb))
	get_node("operate").set_text(str(middleNumb))
	operator = round(rand_range(0,3))
	if operator == 0 :
		get_node("leftNumb").set_text("+")
		get_node("rightNumb").set_text("+")
		answerEnemy = leftNumb + rightNumb + middleNumb
		answerEnemy2 = leftNumb + rightNumb
	if operator == 1 :
		get_node("rightNumb").set_text("-")
		get_node("leftNumb").set_text("-")
		answerEnemy = leftNumb - middleNumb - rightNumb
		answerEnemy2 = leftNumb - middleNumb
	if operator == 2 :
		get_node("rightNumb").set_text("+")
		get_node("leftNumb").set_text("-")
		answerEnemy = leftNumb - middleNumb + rightNumb
		answerEnemy2 = leftNumb - middleNumb
	if operator == 3 :
		get_node("rightNumb").set_text("-")
		get_node("leftNumb").set_text("+")
		answerEnemy = leftNumb + middleNumb - rightNumb
		answerEnemy2 = leftNumb + middleNumb

