extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var opt = 0
var lvl = 0
var score = [0,0,0,0]
var a = 0
var b = 0
var scoreall = 0
onready var music = get_node("/root/var").music

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	while ( a != 12 ):
		score[0] = score[0] + get_node("/root/var").addscore[a]
		score[1] = score[1] + get_node("/root/var").subscore[a]
		score[2] = score[2] + get_node("/root/var").multiscore[a]
		score[3] = score[3] + get_node("/root/var").divscore[a]
		a += 1
	while (b != 4 ):
		scoreall = scoreall + score[b]
		b += 1
	get_node("add/Label").set_text(str(score[0]))
	get_node("subs/Label").set_text(str(score[1]))
	get_node("multi/Label").set_text(str(score[2]))
	get_node("div/Label").set_text(str(score[3]))
	get_node("score/Label").set_text(str(scoreall))
	get_node("music").play("menu")
	
	pass
	


func _on_opt_pressed():
	if opt == 0 :
		get_node("anim").play("option")
		opt = 1
	else :
		get_node("anim").play_backwards("option")
		opt = 0
	pass # replace with function body


func _on_add_pressed():
	get_node("anim").play("add")
	get_node("/root/var").mode = 0
	get_node("add_level").caller()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_add_back_pressed():
	get_node("anim").play_backwards("add")
	get_node("add_level").caller2()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_subs_pressed():
	get_node("anim").play("subs")
	get_node("/root/var").mode = 1
	get_node("add_level").caller()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_multi_pressed():
	get_node("anim").play("multi")
	get_node("/root/var").mode = 2
	get_node("add_level").caller()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_div_pressed():
	get_node("anim").play("div")
	get_node("/root/var").mode = 3
	get_node("add_level").caller()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_end_pressed():
	get_node("anim").play("end")
	get_node("sfx").play("button")
	pass # replace with function body


func _on_div_back_pressed():
	get_node("anim").play_backwards("div")
	get_node("add_level").caller2()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_multi_back_pressed():
	get_node("anim").play_backwards("multi")
	get_node("add_level").caller2()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_subs_back_pressed():
	get_node("anim").play_backwards("subs")
	get_node("add_level").caller2()
	get_node("sfx").play("button")
	pass # replace with function body


func _on_end_back_pressed():
	get_node("anim").play_backwards("end")
	get_node("sfx").play("button")
	pass # replace with function body
	
	
	


func _on_bgm_pressed():
	music = get_node("/root/var").music
	if music == 1 :
		get_node("music").stop_all()
		get_node("/root/var").music = 0
	else :
		get_node("music").play("menu")
		get_node("/root/var").music = 1
	pass # replace with function body
