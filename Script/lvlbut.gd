extends TextureButton

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var i = 3
var a = 0
onready var star = [get_node("star"),get_node("star1"),get_node("star2")]


func _ready():
	# Called every time the node is added to the scene.
	# Initialization here\'
	set_process(true)
	connect("pressed", self, "_button_pressed")
	get_node("../Timer").connect("timeout",self, "_timeout")
	pass
	
#func _process(delta):
#	_countingstars()

func _button_pressed():
	get_node("../sfx").play("sfx")
	get_node("/root/var").lv = get_index() + 1
	get_node("../Timer").start()

func _timeout():
	get_tree().change_scene("res://Scene/load.tscn")

func _countingstars():
	if get_node("/root/var").mode == 0:
		while (i > get_node("/root/var").addscore[get_index()] ):
			star[i - 1].set_texture(load("res://Assets/starlevel3.png"))
			i -= 1
	if get_node("/root/var").mode == 1:
		while (i > get_node("/root/var").subscore[get_index()] ):
			star[i - 1].set_texture(load("res://Assets/starlevel3.png"))
			i -= 1
	if get_node("/root/var").mode == 2:
		while (i > get_node("/root/var").multiscore[get_index()] ):
			star[i - 1].set_texture(load("res://Assets/starlevel3.png"))
			i -= 1
	if get_node("/root/var").mode == 3:
		while (i > get_node("/root/var").divscore[get_index()] ):
			star[i - 1].set_texture(load("res://Assets/starlevel3.png"))
			i -= 1
			
func returnstars():
	a=0
	while ( a != 3 ):
		star[a].set_texture(load("res://Assets/starlevel.png"))
		a+= 1
	i = 3
		
