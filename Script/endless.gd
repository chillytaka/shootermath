extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var condition = [0,0,0,0]

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _on_add_pressed():
	if condition[0] == 0 :
		get_node("anim").play("button_add")
		condition[0] = 1
	else :
		get_node("anim").play_backwards("button_add")
		condition[0] = 0
	pass # replace with function body


func _on_subs_pressed():
	if condition[1] == 0 :
		get_node("anim").play("button_subs")
		condition[1] = 1
	else :
		get_node("anim").play_backwards("button_subs")
		condition[1] = 0
	pass # replace with function body


func _on_multi_pressed():
	if condition[2] == 0 :
		get_node("anim").play("button_multi")
		condition[2] = 1
	else :
		get_node("anim").play_backwards("button_multi")
		condition[2] = 0
	pass # replace with function body


func _on_div_pressed():
	if condition[3] == 0 :
		get_node("anim").play("button_div")
		condition[3] = 1
	else :
		get_node("anim").play_backwards("button_div")
		condition[3] = 0
	pass # replace with function body


func _on_start_pressed():
	pass # replace with function body
