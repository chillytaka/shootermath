
extends Node

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
######################################################
	#check current level
########################################################
	
	if get_node("/root/var").last > 0 :
		get_node("lv2").set_disabled(false)
		get_node("lv2/2").set_opacity(1)
	if get_node("/root/var").last > 1 :
		get_node("lv3").set_disabled(false)
		get_node("lv3/Label").set_opacity(1)
	if get_node("/root/var").last > 2 :
		get_node("lv4").set_disabled(false)
		get_node("lv4/4").set_opacity(1)
	if get_node("/root/var").last > 3 :
		get_node("lv5").set_disabled(false)
		get_node("lv5/5").set_opacity(1)
	if get_node("/root/var").last > 4 :
		get_node("lv6").set_disabled(false)
		get_node("lv6/6").set_opacity(1)
	if get_node("/root/var").last > 5 :
		get_node("lv 7").set_disabled(false)
		get_node("lv 7/1").set_opacity(1)
	if get_node("/root/var").last > 6 :
		get_node("lv 8").set_disabled(false)
		get_node("lv 8/2").set_opacity(1)
	if get_node("/root/var").last > 7 :
		get_node("lv 9").set_disabled(false)
		get_node("lv 9/Label").set_opacity(1)
	pass


func _on_Button_pressed():
	if get_node("anim").get_current_animation() == "lv2" and get_node("anim").get_current_animation_pos() == 0.5:
		get_node("anim").play_backwards("lv2")
	else :
		get_node("../anim").play_backwards("level")
	pass # replace with function body

func _on_Button1_pressed():
	if get_node("anim").get_current_animation() == "lv2" and get_node("anim").get_current_animation_pos() == 0.5 :
		pass
	else :
		get_node("anim").play("lv2")
	pass # replace with function body


func _on_lv1_pressed():
	get_node("/root/var").lv = 1
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 1
	pass # replace with function body


func _on_lv2_pressed():
	get_node("/root/var").lv = 2
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 2
	pass # replace with function body


func _on_lv3_pressed():
	get_node("/root/var").lv = 3
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 3
	pass # replace with function body


func _on_lv4_pressed():
	get_node("/root/var").lv = 4
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 4
	pass # replace with function body


func _on_lv5_pressed():
	get_node("/root/var").lv = 5
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 5
	pass # replace with function body


func _on_lv6_pressed():
	get_node("/root/var").lv = 6
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 6
	pass # replace with function body


func _on_lv_7_pressed():
	get_node("/root/var").lv = 7
	get_tree().change_scene("res://load.tscn")
	get_node("/root/var").last = 7
	pass # replace with function body


func _on_lv_8_pressed():
	if get_node("/root/var").last > 6 :
		get_node("/root/var").lv = 8
		get_tree().change_scene("res://load.tscn")
		get_node("/root/var").last = 8
	else :
		get_node("popUp").show()
	pass # replace with function body


func _on_lv_9_pressed():
	if get_node("/root/var").last > 7 :
		get_node("/root/var").lv = 9
		get_tree().change_scene("res://load.tscn")
		get_node("/root/var").last = 9
	else :
		get_node("popUp").show()
	pass # replace with function body


func _on_lv_10_pressed():
	if get_node("/root/var").last > 8 :
		get_node("/root/var").lv = 10
		get_tree().change_scene("res://load.tscn")
		get_node("/root/var").last = 10
	else :
		get_node("popUp").show()
	pass # replace with function body


func _on_lv_11_pressed():
	if get_node("/root/var").last > 9 :
		get_node("/root/var").lv = 11
		get_tree().change_scene("res://load.tscn")
		get_node("/root/var").last = 11
	else :
		get_node("popUp").show()
	pass # replace with function body


func _on_lv_12_pressed():
	if get_node("/root/var").last > 10 :
		get_node("/root/var").lv = 12
		get_tree().change_scene("res://load.tscn")
		get_node("/root/var").last = 12
	else :
		get_node("popUp").show()
	pass # replace with function body


func _on_help_pressed():
	pass # replace with function body
