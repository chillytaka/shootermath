extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("anim").play("over")
	pass


func _on_home_pressed():
	get_tree().change_scene("res://menu.tscn")
	get_tree().set_pause(false)
	pass # replace with function body

func _on_restart_pressed():
	get_tree().change_scene("res://load.tscn")
	get_tree().set_pause(false)
	pass # replace with function body
