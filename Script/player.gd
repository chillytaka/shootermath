
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar
var empty = true
var complete = false
var answer
var enemy = 1
var enemyList = []
var yourEnemy = preload("res://Scene/enemy.tscn")
var enemyCount = 0
var benar = false
var bullet = preload("res://Scene/Bullet.tscn")
var shoot = false
var enemyDestroy = 0
var win = false
var bgm = 0
var admob=null

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	randomize()
	get_tree().set_pause(false)
	set_process(true)
	get_node("../Timer").start()
	get_node("../anim").play("start")
	if (get_node("/root/var").lv == 0):
		get_node("end").start()
	
###################################################
	#audio setting
###################################################3
	if get_node("/root/var").music == 1 :
		bgm = round(rand_range(1,2))
		if bgm == 1 :
			get_node("../sfx").play("bgm2")
		if bgm == 2 :
			get_node("../sfx").play("bgm3")
		
	#handling quit button
	get_tree().set_auto_accept_quit(false)
	get_node("/root/var").life = 3
	get_tree().connect("screen_resized",self,"update_size")
	
	#ad
	if(Globals.has_singleton("AdMob")):
		admob = Globals.get_singleton("AdMob")
		admob.init(false, false, get_node("/root/var").ad_id)
		admob.showBanner(true)
#ad
func update_size():
	if(admob != null):
		print(admob.getAdWidth(), ", ", admob.getAdHeight())
		admob.resize(false)
	pass

func _process(delta):
	var pos = Vector2()
	pos = get_pos()
	
	
	#move to enemy
	if benar :
		if ( pos.x > get_node("../" + str(enemy + 1)).get_pos().x ) :
			pos += Vector2(-4,0)
			set_pos(pos)
			
			#spawn bullet
			if (pos.x - get_node("../" + str(enemy + 1)).get_pos().x < 3):
				var spawn = bullet.instance()
				spawn.set_pos(get_node(".").get_pos())
				get_node("../").add_child(spawn)
				benar = false
		
		
		if ( pos.x < get_node("../" + str(enemy + 1)).get_pos().x ) :
			pos += Vector2(4,0)
			set_pos(pos)
			
	#limiting enemy number
	if get_node("/root/var").lv != 0 :
		if enemyCount == 25 :
			get_node("../Timer").stop()
			
			#check if enemy destroyed
			if enemyList[24] == 0 :
				set_process(false)
				get_node("../anim").play("win")

	pass
	
#set star received
func _on_anim_finished():
	if (get_node("../anim").get_current_animation()) == "win" :
		get_node("../ui 2/win").win()
	pass # replace with function body


func _on_fire_pressed():
	answer = int(get_node("answer1").get_text()) * 10 + int(get_node("answer2").get_text())
	get_node("answer1").set_text("0")
	get_node("answer2").set_text("0")
	empty = true
	complete = false
	
	#check answer
	if enemyList.find(answer) != -1 and not answer == 0 :
		enemy = enemyList.find(answer,0)
		benar = true
	pass # replace with function body

func _on_0_pressed():
	if empty:
		get_node("answer2").set_text("0")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("0")
	#get_node("../anim").play("win")
	pass # replace with function body


func _on_1_pressed():
	if empty:
		get_node("answer2").set_text("1")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("1")
	pass # replace with function body


func _on_2_pressed():
	if empty:
		get_node("answer2").set_text("2")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("2")
	pass # replace with function body


func _on_3_pressed():
	if empty:
		get_node("answer2").set_text("3")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("3")
	pass # replace with function body


func _on_4_pressed():
	if empty:
		get_node("answer2").set_text("4")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("4")
	pass # replace with function body


func _on_5_pressed():
	if empty:
		get_node("answer2").set_text("5")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("5")
		complete = true
	pass # replace with function body


func _on_6_pressed():
	if empty:
		get_node("answer2").set_text("6")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("6")
		complete = true
	pass # replace with function body


func _on_7_pressed():
	if empty:
		get_node("answer2").set_text("7")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("7")
		complete = true
	pass # replace with function body


func _on_8_pressed():
	if empty:
		get_node("answer2").set_text("8")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("8")
		complete = true
	pass # replace with function body


func _on_9_pressed():
	if empty:
		get_node("answer2").set_text("9")
		empty = false
	else :
		get_node("answer1").set_text(get_node("answer2").get_text())
		get_node("answer2").set_text("9")
		complete = true
	pass # replace with function body


func _on_Timer_timeout():
	var spawner = yourEnemy.instance()
	var y = get_node("../y").get_pos()
	var x = get_node ("../x").get_pos()
	spawner.set_pos(Vector2(rand_range(60,x.x),y.y))
	spawner.set_name(str(enemyCount + 1))
	get_node("../Timer").start()
	get_node("../").add_child(spawner)
	pass # replace with function body


func _on_end_timeout():
	get_node("/root/var").loop += 1
	get_node("/root/var").maxNumb += 5
	get_node("end").start()
	if get_node("/root/var").loop == 3 :
		get_node("/root/var").maxNumb = 4
	if get_node("/root/var").loop % 2 :
		get_node("/root/var").speed += 0.5
	pass # replace with function body
