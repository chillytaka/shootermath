extends Sprite

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	get_node("Sprite").hide()
	get_node("Label").show()
	if (get_node("/root/var").lv != 0 ):
		get_node("Label").set_text(str(get_node("/root/var").lv))
	else :
		get_node("Label").hide()
		get_node("Sprite").show()
	pass
