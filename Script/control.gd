extends Control

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass
	
func caller():
	get_node("lv1")._countingstars()
	get_node("lv2")._countingstars()
	get_node("lv3")._countingstars()
	get_node("lv4")._countingstars()
	get_node("lv5")._countingstars()
	get_node("lv6")._countingstars()
	get_node("lv7")._countingstars()
	get_node("lv8")._countingstars()
	get_node("lv9")._countingstars()
	get_node("lv10")._countingstars()
	get_node("lv11")._countingstars()
	get_node("lv12")._countingstars()
	
func caller2():
	get_node("lv1").returnstars()
	get_node("lv2").returnstars()
	get_node("lv3").returnstars()
	get_node("lv4").returnstars()
	get_node("lv5").returnstars()
	get_node("lv6").returnstars()
	get_node("lv7").returnstars()
	get_node("lv8").returnstars()
	get_node("lv9").returnstars()
	get_node("lv10").returnstars()
	get_node("lv11").returnstars()
	get_node("lv12").returnstars()
	
