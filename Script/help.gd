extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var frame_pos = 0

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass


func _on_back_pressed():
	get_node("../anim").play_backwards("help")
	pass # replace with function body


func _on_next_pressed():
	frame_pos = get_node("Sprite").get_frame()
	if frame_pos != 8 :
		get_node("Sprite").set_frame( frame_pos + 1 )
		get_node("page").set_text(str( int(get_node("page").get_text()) + 1))
	pass # replace with function body


func _on_prev_pressed():
	frame_pos = get_node("Sprite").get_frame()
	if frame_pos != 0 :
		get_node("Sprite").set_frame( frame_pos - 1 )
		get_node("page").set_text(str( int(get_node("page").get_text()) - 1))
	pass # replace with function body
