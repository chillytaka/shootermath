
extends Control

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	pass

func win():
	if get_node("../../anim").is_playing()== false :
		if get_node("/root/var").life == 1 :
			get_node("animate").play("star1")
		if get_node("/root/var").life == 2 :
			get_node("animate").play("star2")
		if get_node("/root/var").life == 3 :
			get_node("animate").play("star3")


func _on_home_pressed():
	get_tree().change_scene("res://menu.tscn")
	pass # replace with function body


func _on_restart_pressed():
	get_tree().change_scene("res://load.tscn")
	pass # replace with function body


func _on_next_pressed():
	get_node("/root/var").lv+= 1
	get_tree().change_scene("res://main.tscn")
	pass # replace with function body
