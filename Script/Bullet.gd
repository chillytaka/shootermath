
extends Node2D

# member variables here, example:
# var a=2
# var b="textvar"
var pos = Vector2()

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process(true)
	pass
	
func _process(delta):
	pos = get_pos()
	pos += Vector2(0,-20)
	set_pos(pos)




func _on_Bullet_area_enter( area ):
	if (area.has_method("_on_enemy_area_enter")):
		queue_free()
	pass # replace with function body
